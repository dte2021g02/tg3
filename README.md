# TG3
### ¿Cual es el objetivo de este grupo?

Proyecto para llevar a cabo el trabajo en grupo, del grupo 02, de la asignatura Desarrollo con Tecnologías Emergentes de grado Ingeniería en Sistemas de Información en la UAH.

### ¿Quienes componen el grupo?

- Daniel Sanz Mena
- Lucas Jorge Espejo
- Víctor Alba Muñiz
- Alejandro Martín Merino
- Jorge Sánchez Sánchez



### Nuestro trabajo
Puede visitar la web del grupo a través de este enlace: https://dte2021g02.gitlab.io/tg3/tg3.html
